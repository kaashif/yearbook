#!/usr/bin/env python3.4
import mailbox, os, re
from subprocess import call
from random import randrange
from tempfile import mkstemp

inbox = mailbox.Maildir("/home/yearbook/mail")
archive = mailbox.Maildir("/home/yearbook/archive")

def send_to_archive(key, message):
    archive.add(message)
    inbox.discard(key)

def print_info(message):
    print("\nsubject: %s" % message['subject'])    
    print("from: %s" % message['from'])

def has_images(message):
    if not message.is_multipart():
        return False
    for part in message.get_payload():
        if 'image' == part.get_content_maintype():
            return True

def upload_to_dropbox(image, sender):
    filename = "%s-%s-%s" % (
        sender,
        str(randrange(10000, 99999)),
        image.get_filename()
    )
    localimg = mkstemp()[1]
    open(localimg, 'wb').write(image.get_payload(decode=1))
    print("uploading image %s" % filename)
    call(["/home/yearbook/dropbox_uploader.sh", "upload", localimg, filename])
    os.remove(localimg)

def upload_images(message):
    parts = message.get_payload()
    images = filter(lambda p: p.get_content_maintype() == 'image', parts)
    sender = message['from'].replace(' ', '_')
    for image in images:
        upload_to_dropbox(image, sender)

def has_keyword(message):
    return re.search('[Yy]earbook', message['subject'])
        
for key, message in inbox.iteritems():
    send_to_archive(key, message)
    print_info(message)
    if not has_images(message):
        print("no images")
        continue
        
    if not has_keyword(message):
        print("no keyword")
        continue

    upload_images(message)

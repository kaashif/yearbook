Yearbook Uploading Script
=========================

Instructions
------------
1. Create a user named 'yearbook': `useradd -m yearbook`
2. Make 2 maildirs named "archive" and "mail": `mkdir -p ~yearbook/{archive,mail}/{cur,new,tmp}`
3. Copy the scripts: `cp ./{yearbook.py,dropbox_uploader.sh} ~yearbook/`
4. Follow the instructions to setup the Dropbox uploader (run this as yearbook): `./dropbox_uploader.sh`
5. Setup a mail delivery system to get mail into ~/mail somehow (getmail, fetchmail, something else)
6. Add cron jobs to run getmail (for example) then yearbook.py

